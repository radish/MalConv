#!/usr/bin/env python3

import click


@click.group()
def cli():
    pass


@cli.command()
@click.argument('model_name', type=str)
@click.argument('training_data_dir', type=click.Path(exists=True, file_okay=False))
@click.argument('validation_data_dir', type=click.Path(exists=True, file_okay=False))
@click.argument('labels_file', type=click.Path(exists=True, dir_okay=False))
@click.argument('model_file', type=click.Path(dir_okay=False))
@click.argument('tensorboard_dir', type=click.Path(exists=True, file_okay=False))
@click.option('--batch-size', type=int, default=2)
@click.option('--epochs', type=int, default=30)
def train(model_name: str, training_data_dir: str, validation_data_dir: str, labels_file: str, model_file: str,
          tensorboard_dir: str, batch_size: int, epochs: int):
    import os
    import datetime
    import importlib
    from malconv.generators.raw_bytes import MalwareSequence
    from tensorflow.python.keras.callbacks import ModelCheckpoint, TensorBoard

    get_model = getattr(importlib.import_module(f'malconv.models.{model_name}'), 'get_model')

    max_executable_length = 8388608

    training_sequence = MalwareSequence(training_data_dir, labels_file, max_executable_length, batch_size)
    validation_sequence = MalwareSequence(validation_data_dir, labels_file, max_executable_length, batch_size)

    checkpointer = ModelCheckpoint(model_file, verbose=1, save_best_only=True)
    tensorboard = TensorBoard(os.path.join(tensorboard_dir, str(datetime.datetime.now())))

    model = get_model(training_sequence.classes_weights, max_executable_length)
    model.fit_generator(generator=training_sequence, epochs=epochs, validation_data=validation_sequence,
                        callbacks=[checkpointer, tensorboard], max_queue_size=batch_size * 2, use_multiprocessing=True,
                        workers=2)


@cli.command()
@click.argument('test_data_dir', type=click.Path(exists=True, file_okay=False))
@click.argument('model_file', type=click.Path(dir_okay=False))
@click.argument('output_file', type=click.Path(dir_okay=False))
def predict(test_data_dir: str, model_file: str, output_file: str):
    import os
    import numpy as np
    import pickle
    from malconv.loaders.optimized_bytes import read_bytes_from_file
    from malconv.models.malconv_512 import decov_loss, weighted_categorical_crossentropy
    from tensorflow.python.keras.models import Model, load_model

    def get_regularization():
        return decov_loss

    def get_loss():
        return weighted_categorical_crossentropy(
            np.array(
                [0.02725503, 0.01694915, 0.014276, 0.08842105, 1., 0.05592543, 0.10552764, 0.03420195, 0.04146101]
            )
        )

    max_executable_length = 8388608
    model: Model = load_model(model_file, custom_objects={'decov_loss': get_regularization,
                                                          'loss': get_loss()})

    files_count = len(os.listdir(test_data_dir))

    y_pred = {}
    for i, file in enumerate(os.scandir(test_data_dir)):
        print(f'\r{i + 1}/{files_count}', end='')
        bytes_array = read_bytes_from_file(file.path)
        input_batch = np.full(shape=(1, max_executable_length), fill_value=257, dtype=np.uint16)
        input_batch[0][:len(bytes_array)] = bytes_array[:max_executable_length]
        y_pred[os.path.splitext(file.name)[0]] = np.squeeze(model.predict(input_batch, batch_size=1))

    with open(output_file, mode='wb') as file:
        pickle.dump(y_pred, file)


@cli.command()
@click.argument('predictions_file', type=click.Path(dir_okay=False))
@click.argument('labels_file', type=click.Path(exists=True, dir_okay=False))
def get_metrics(predictions_file: str, labels_file: str):
    import os
    import numpy as np
    import pickle
    from malconv.loaders.labels import load_labels
    from tensorflow.python.keras.utils import to_categorical
    from sklearn.metrics import log_loss, accuracy_score, classification_report, roc_auc_score, confusion_matrix

    labels = load_labels(labels_file)
    class_weights = [0.02725503, 0.01694915, 0.014276, 0.08842105, 1., 0.05592543, 0.10552764, 0.03420195, 0.04146101]

    with open(predictions_file, mode='rb') as input_file:
        predictions = pickle.load(input_file)

    files_count = len(predictions)
    print(files_count)

    y_true_labels = np.ndarray((files_count,), dtype=np.float32)
    y_pred_labels = np.ndarray((files_count,), dtype=np.float32)
    y_true = np.ndarray((files_count, 9), dtype=np.float32)
    y_pred = np.ndarray((files_count, 9), dtype=np.float32)
    y_pred_argmax = np.ndarray((files_count, 9), dtype=np.float32)
    sample_weight = []
    for i, (file_name, prediction) in enumerate(predictions.items()):
        label = labels[os.path.splitext(file_name)[0]]
        y_true_labels[i] = label
        y_pred_labels[i] = np.argmax(prediction)
        y_true[i] = to_categorical(label, num_classes=9)
        y_pred[i] = prediction
        y_pred_argmax[i] = to_categorical(np.argmax(prediction), num_classes=9)
        sample_weight.append(class_weights[label])

    print(f'\nFinal log loss: {log_loss(y_true, y_pred)}')
    print(f'Final log loss with argmax: {log_loss(y_true, y_pred_argmax)}')
    print(f'Final weighted log loss: {log_loss(y_true, y_pred, sample_weight=sample_weight)}')
    print(f'Final weighted log loss with argmax: {log_loss(y_true, y_pred_argmax, sample_weight=sample_weight)}')

    print(f'\nFinal accuracy: {accuracy_score(y_true_labels, y_pred_labels)}')
    print(f'Final weighted accuracy: {accuracy_score(y_true_labels, y_pred_labels, sample_weight=sample_weight)}')

    print(f'\nFinal ROC AUC: {roc_auc_score(y_true, y_pred)}')
    print(f'Final weighted ROC AUC: {roc_auc_score(y_true, y_pred, sample_weight=sample_weight)}')

    print(f'\n{classification_report(y_true_labels, y_pred_labels)}')

    print(f'\n{confusion_matrix(y_true_labels, y_pred_labels)}')


@cli.command()
@click.argument('training_data_dir', type=click.Path(exists=True, file_okay=False))
@click.argument('validation_data_dir', type=click.Path(exists=True, file_okay=False))
@click.argument('labels_file', type=click.Path(exists=True, dir_okay=False))
@click.argument('validation_size', type=float)
def make_validation_dataset(training_data_dir: str, validation_data_dir: str, labels_file: str, validation_size: float):
    import os
    from sklearn.model_selection import train_test_split
    from malconv.loaders.labels import load_labels
    from collections import defaultdict

    labels = load_labels(labels_file)

    files_in_categories = defaultdict(lambda: list())
    for file_name, category in labels.items():
        files_in_categories[category].append(file_name)

    validation_paths = []
    for _, files in files_in_categories.items():
        validation_paths.extend(train_test_split(files, test_size=validation_size)[1])

    for validation_path in validation_paths:
        file_name = os.path.splitext(os.path.basename(validation_path))[0]
        bytes_file_name = f'{file_name}.bytes'
        optimized_file_name = f'{file_name}.optimized'
        asm_file_name = f'{file_name}.asm'

        os.rename(os.path.join(training_data_dir, bytes_file_name),
                  os.path.join(validation_data_dir, bytes_file_name))
        os.rename(os.path.join(training_data_dir, optimized_file_name),
                  os.path.join(validation_data_dir, optimized_file_name))
        os.rename(os.path.join(training_data_dir, asm_file_name),
                  os.path.join(validation_data_dir, asm_file_name))


@cli.command()
@click.argument('data_dir', type=click.Path(exists=True, file_okay=False))
def optimize_files(data_dir: str):
    import os
    import glob
    from malconv.loaders.raw_bytes import read_bytes_from_file

    paths = glob.glob(os.path.join(data_dir, '*.bytes'))
    paths_len = len(paths)
    for i, path in enumerate(paths):
        print(f'\r{i + 1}/{paths_len}', end='')
        file_name = os.path.splitext(os.path.basename(path))[0]
        try:
            read_bytes_from_file(path).tofile(os.path.join(data_dir, f'{file_name}.optimized'))
        except UnicodeDecodeError:
            print(f'\nError loading file {path}')


if __name__ == '__main__':
    cli()
