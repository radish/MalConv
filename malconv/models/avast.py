import numpy as np
from tensorflow.python.keras import Model, Input
from tensorflow.python.keras.activations import relu, selu, softmax
from tensorflow.python.keras.layers import Embedding, Conv1D, GlobalAveragePooling1D, MaxPooling1D, Dense
from tensorflow.python.keras.losses import categorical_crossentropy
from tensorflow.python.keras.metrics import categorical_accuracy
from tensorflow.python.keras.optimizers import Adam

from malconv.utils import weighted_categorical_crossentropy


def get_model(weights: np.ndarray, executable_length: int) -> Model:
    input_layer = Input(shape=(executable_length,), dtype=np.uint16)
    x = Embedding(input_dim=258, output_dim=8, input_length=executable_length)(input_layer)
    x = Conv1D(filters=48, kernel_size=32, strides=4, activation=relu)(x)
    x = Conv1D(filters=96, kernel_size=32, strides=4, activation=relu)(x)
    x = MaxPooling1D(pool_size=4)(x)
    x = Conv1D(filters=128, kernel_size=16, strides=8, activation=relu)(x)
    x = Conv1D(filters=192, kernel_size=16, strides=8, activation=relu)(x)
    x = GlobalAveragePooling1D()(x)
    x = Dense(units=192, activation=selu, kernel_initializer='random_normal')(x)
    x = Dense(units=160, activation=selu, kernel_initializer='random_normal')(x)
    x = Dense(units=128, activation=selu, kernel_initializer='random_normal')(x)
    x = Dense(units=9, activation=softmax)(x)

    model = Model(input_layer, x)
    model.compile(optimizer=Adam(),
                  loss=weighted_categorical_crossentropy(weights), metrics=[categorical_accuracy,
                                                                            categorical_crossentropy])
    model.summary()

    return model
