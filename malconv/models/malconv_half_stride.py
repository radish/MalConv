import numpy as np
from tensorflow.python.keras import Model, Input
from tensorflow.python.keras.activations import sigmoid, selu, softmax
from tensorflow.python.keras.layers import Embedding, Conv1D, Multiply, GlobalMaxPooling1D, Dense
from tensorflow.python.keras.losses import categorical_crossentropy
from tensorflow.python.keras.metrics import categorical_accuracy
from tensorflow.python.keras.optimizers import SGD

from malconv.utils import weighted_categorical_crossentropy, decov_loss


def get_model(weights: np.ndarray, executable_length: int) -> Model:
    input_layer = Input(shape=(executable_length,), dtype=np.uint16)
    x = Embedding(input_dim=258, output_dim=8, input_length=executable_length)(input_layer)
    conv_gate = Conv1D(filters=256, kernel_size=512, strides=256)(x)
    conv_sigmoid = Conv1D(filters=256, kernel_size=512, strides=256, activation=sigmoid)(x)
    x = Multiply()([conv_gate, conv_sigmoid])
    x = GlobalMaxPooling1D()(x)
    x = Dense(units=256, activation=selu, activity_regularizer=decov_loss)(x)
    x = Dense(units=9, activation=softmax)(x)

    model = Model(input_layer, x)
    model.compile(optimizer=SGD(lr=0.001, momentum=0.9, decay=1e-6, nesterov=True),
                  loss=weighted_categorical_crossentropy(weights), metrics=[categorical_accuracy,
                                                                            categorical_crossentropy])

    model.summary()

    return model
