import numpy as np
import tensorflow as tf
from tensorflow.python.keras import backend as K


def decov_loss(x):
    z = tf.expand_dims(x - tf.reduce_mean(x, 0, True), 2)
    corr = tf.reduce_mean(tf.matmul(z, tf.transpose(z, perm=[0, 2, 1])), 0)
    corr_frob_sqr = tf.reduce_sum(tf.square(corr))
    corr_diag_sqr = tf.reduce_sum(tf.square(tf.diag_part(corr)))
    return 0.5 * (corr_frob_sqr - corr_diag_sqr)


def weighted_categorical_crossentropy(weights: np.ndarray):
    weights = K.variable(weights)

    def loss(y_true, y_pred):
        # scale predictions so that the class probas of each sample sum to 1
        y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
        # clip to prevent NaN's and Inf's
        y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
        # calc
        loss_value = y_true * K.log(y_pred) * weights
        loss_value = -K.sum(loss_value, -1)
        return loss_value

    return loss
