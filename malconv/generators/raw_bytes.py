import math

import glob
import numpy as np
import os
from tensorflow.python.keras.utils import Sequence
from tensorflow.python.keras.utils import to_categorical
from typing import List, Tuple

from malconv.loaders.labels import load_labels
from malconv.loaders.optimized_bytes import read_bytes_from_file


class MalwareSequence(Sequence):
    def __init__(self, data_directory_path: str, labels_file_path: str, input_length: int, batch_size: int):
        self._paths: List[str] = glob.glob(os.path.join(data_directory_path, '*.optimized'))
        self._labels = load_labels(labels_file_path)
        self._input_length = input_length
        self._batch_size = batch_size

        self.classes_weights = np.zeros((9,), dtype=np.float32)
        for _, value in self._labels.items():
            self.classes_weights[value] += 1
        self.classes_weights = np.min(self.classes_weights) / self.classes_weights

    def __len__(self) -> int:
        return int(math.ceil(len(self._paths) / self._batch_size))

    def __getitem__(self, index) -> Tuple[np.ndarray, np.ndarray]:
        input_batch = np.full(shape=(self._batch_size, self._input_length), fill_value=257, dtype=np.uint16)
        output_batch = np.ndarray((self._batch_size, 9), dtype=np.float32)

        input_batch_paths = self._paths[index * self._batch_size:(index + 1) * self._batch_size]
        for position_in_batch, path in enumerate(input_batch_paths):
            bytes_array = read_bytes_from_file(path)
            input_batch[position_in_batch][:len(bytes_array)] = bytes_array[:self._input_length]

            file_id = os.path.splitext(os.path.basename(path))[0]
            output_batch[position_in_batch] = to_categorical(self._labels[file_id], num_classes=9)

        return input_batch, output_batch

    def on_epoch_end(self):
        pass
