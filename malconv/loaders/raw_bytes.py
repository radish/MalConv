import numpy as np
import pandas as pd


def read_bytes_from_file(path: str) -> np.ndarray:
    return pd.read_csv(path, sep=' ', header=None, names=[i for i in range(0, 17)],
                       usecols=[i for i in range(1, 17)],
                       dtype=str, low_memory=False).applymap(_convert_to_int).astype(np.uint16).values.flatten()


def _convert_to_int(value: str) -> int:
    try:
        return int(value, 16)
    except ValueError:
        # invalid ?? opcode meaning reserved space
        return 256
    except TypeError:
        # nan value meaning lack of opcodes at the eof
        return 257
