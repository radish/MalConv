from typing import Dict

import pandas as pd


def load_labels(file_path: str) -> Dict[str, int]:
    return pd.read_csv(file_path, index_col=0, squeeze=True).apply(lambda x: x - 1).to_dict()
