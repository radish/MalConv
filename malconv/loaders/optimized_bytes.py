import numpy as np


def read_bytes_from_file(path: str) -> np.ndarray:
    return np.fromfile(path, dtype=np.uint16)
